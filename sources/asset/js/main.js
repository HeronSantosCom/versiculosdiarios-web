$(document).ready(function() {
    
    if ($('#chamada-chrome').length > 0) {
        $('#chamada-chrome').popover({
            trigger: "hover",
            placement: "bottom",
            content: "Baixa já o aplicativo para Google Chrome no <a href='http://curte.eu/vd-chrome/' target='_blank' title='Clique aqui!'><b>Chrome Web Store</b></a>!"
        }).popover('show');
    }
    
    setTimeout(function() {
        $('#chamada-chrome').popover('hide');
    }, 3000);
    
});